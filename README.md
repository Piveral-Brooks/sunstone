# Sunstone

Ruby script for displaying data from OpenWeather in terminal.


## Datasources

- [OpenCage Geocoder](https://opencagedata.com/) for location information
- [OpenWeatherMap](https://openweathermap.org/) for weather

## Contributors

- Keenan Piveral-Brooks (Maintainer)
- [Alex Gittemeier](https://a.lexg.dev/)
