# frozen_string_literal: true

require 'json'
require 'rest-client'

module Sunstone
  Location = Struct.new(:lat, :lon, :name) do
    @@cache = Sunstone::Cache.load(File.expand_path('../cache.json', __dir__))
    at_exit { @@cache.save }

    def self.query(query)
      @@cache['locations'] ||= {}
      @@cache['locations'][query] = query_opencagedata(query) unless @@cache['locations'].key?(query)

      Location.new(*@@cache['locations'][query])
    end

    def self.query_opencagedata(query)
      puts Rainbow('(Fetching data from opencagedata)').bright.black
      response = RestClient.get('https://api.opencagedata.com/geocode/v1/json', params: {
        q: query, key: Secrets.opencagedata_key, limit: 1, no_annotations: 1, no_record: 1
      })

      result = JSON.parse(response.body)['results'].first
      [result['geometry']['lat'], result['geometry']['lng'], result['formatted']]
    rescue RestClient::Unauthorized
      raise CLIError, %(The "opencagedata" key in secrets.json is invalid.\n\n#{Secrets::OPENCAGEDATA_INSTRUCTIONS})
    end
  end
end
