# frozen_string_literal: true

require 'json'
require 'rainbow'
require 'rest-client'

module Sunstone
  class Weather
    def initialize(location, units: 'imperial')
      puts Rainbow('(Fetching data from openweathermap)').bright.black
      response = RestClient.get('https://api.openweathermap.org/data/2.5/onecall', params: {
        lat: location.lat, lon: location.lon, units: units, appid: Secrets.openweathermap_key
      })

      @weather = JSON.parse(response.body)
      @location = location
    rescue RestClient::Unauthorized
      raise CLIError, %(The "openweathermap" key in secrets.json is invalid.\n\n#{Secrets::OPENWEATHERMAP_INSTRUCTIONS})
    end

    class << self
      alias at new
    end

    attr_reader :location, :weather

    def print_hourly
      dataset = @weather['hourly']
      temp_minmax = dataset.map {|x| x['temp'] }.minmax

      puts @location.name
      puts
      dataset.each do |x|
        datetime = Time.at(x['dt']).strftime('%b %_d  %_I %P')
        puts format_hourly_bar(datetime,
          x['weather'].first['description'],
          x['temp'],
          temp_minmax,
          Rainbow('█').cyan,
          Rainbow('░').blue)
      end
    end

    def print_daily
      dataset = @weather['daily']
      min_min = dataset.map {|x| x['temp']['min'] }.min
      max_max = dataset.map {|x| x['temp']['max'] }.max

      puts @location.name
      puts
      dataset.each do |x|
        datetime = Time.at(x['dt']).strftime('%b %_d')
        puts format_daily_bar(
          datetime,
          x['weather'].first['description'],
          [x['temp']['min'], x['temp']['max']],
          [min_min, max_max],
          Rainbow('█').cyan,
          Rainbow('░').blue
        )
        puts
      end
    end

    def print_minutely
      dataset = @weather['minutely']
      precip_minmax = dataset.map {|x| x['precipitation'] }.minmax

      dataset.each do |x|
        datetime = Time.at(x['dt']).strftime('%b %_d  %_I:%M %P')
        puts format_minutely_bar(
          datetime,
          x['precipitation'],
          precip_minmax,
          Rainbow('█').cyan,
          Rainbow('░').blue
        )
      end
    end

    def format_hourly_bar(time, condition, value, (min, max), filled_char, unfilled_char, width = 30)
      increment = (max - min).fdiv(width)
      filled_length = (value - min).fdiv(increment).round
      bar =  filled_char * (0..filled_length).size
      bar << unfilled_char * (filled_length..width).size

      "#{time} #{bar} #{value.round(1)}° #{condition}"
    end

    def format_daily_bar(time, condition, (min, max), (min_min, max_max), filled_char, unfilled_char, width = 30)
      increment = (max_max - min_min).fdiv(width)
      filled_start = (min - min_min).fdiv(increment).round
      filled_end = (max - min_min).fdiv(increment).round
      bar =  unfilled_char * (0..filled_start).size
      bar << filled_char * (filled_start...filled_end).size
      bar << unfilled_char * (filled_end..width).size

      "#{time} #{min.round}° #{bar} #{max.round}° #{condition}"
    end

    def format_minutely_bar(time, value, (min, max), filled_char, unfilled_char, width = 30)
      if min == 0
        bar = unfilled_char * width
      else
        increment = (max - min).fdiv(width)
        filled_length = (value - min).fdiv(increment).round
        bar =  filled_char * (0..filled_length).size
        bar << unfilled_char * (filled_length..width).size
      end
      "#{time} #{bar} #{value}mm"
    end
  end
end
