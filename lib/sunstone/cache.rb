# frozen_string_literal: true

require 'json'

module Sunstone
  class Cache < Hash
    def self.load(filename, default = {})
      Cache.new(filename, JSON.parse(File.read(filename)))
    rescue Errno::ENOENT
      Cache.new(filename, default)
    end

    def initialize(filename, hash)
      merge!(hash)
      @filename = filename
    end

    def save
      File.write(@filename, to_json)
    end
  end
end
