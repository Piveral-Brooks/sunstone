# frozen_string_literal: true

require 'json'

module Sunstone
  module Secrets
    SECRETS_JSON_PATH = File.expand_path('../../secrets.json', __dir__)

    OPENCAGEDATA_INSTRUCTIONS = <<~HOW_TO.chop
      - Sign up for OpenCage at https://opencagedata.com/users/sign_up
      - Go to https://opencagedata.com/dashboard#geocoding
      - Create an API key and copy it into #{SECRETS_JSON_PATH}
    HOW_TO

    OPENWEATHERMAP_INSTRUCTIONS = <<~HOW_TO.chop
      - Sign up for OpenWeather at https://home.openweathermap.org/users/sign_up
      - Go to https://home.openweathermap.org/api_keys
      - Create an API key and copy it into #{SECRETS_JSON_PATH}
    HOW_TO

    class << self
      def opencagedata_key
        keys.fetch(:opencagedata)
      end

      def openweathermap_key
        keys.fetch(:openweathermap)
      end

      private

      def keys
        @keys ||= JSON.parse(File.read(SECRETS_JSON_PATH), symbolize_names: true).fetch(:api_keys)
      rescue Errno::ENOENT
        File.write(SECRETS_JSON_PATH, JSON.pretty_generate(template_secrets))
        raise CLIError, <<~MSG
          Sunstone requires API keys to be configured.

          OpenCage:
          #{OPENCAGEDATA_INSTRUCTIONS}

          OpenWeather:
          #{OPENWEATHERMAP_INSTRUCTIONS}

          The file should look like this when you are finished:

          #{JSON.pretty_generate(template_secrets('0123456789abcdef0123456789abcdef'))}
        MSG
      end

      def template_secrets(placeholder = '')
        {
          api_keys: {
            opencagedata: placeholder,
            openweathermap: placeholder,
          },
        }
      end
    end
  end
end
