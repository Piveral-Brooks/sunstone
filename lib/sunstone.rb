# frozen_string_literal: true

module Sunstone
  CLIError = Class.new(StandardError)
end

require_relative 'sunstone/cache'
require_relative 'sunstone/location'
require_relative 'sunstone/secrets'
require_relative 'sunstone/weather'
